package com.example.jeanclaude.tp_qrcode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // on teste tout d'abord si le package pour décoder les QRCodes est bien installé
        // si non, on propose de l'installer
        // On pourrait aussi utiliser PackageManager pm = getPackageManager(); pour tester ce genre de choses
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            // optionnel (pour fixer un package précis)
            // intent.setPackage("com.google.zxing.client.android");

            // pour lire un QRCode
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            // par défaut, Scan.ACTION scanne tout type de code barre
            // Intent.putExtra("SCAN_MODE", value) :
            // - PRODUCT_MODE: uniquement UPC et EAN (produits, revues, ...).
            //      ex: intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
            // - ONE_D_MODE: code barre 1D ( UPC, EAN, Code 39,Code 128).
            // - QR_CODE_MODE: uniquement QR codes

            // pour lire un code barre normal
//             intent.putExtra("SCAN_MODE", "PRODUCT_MODE");

            // Source: http://www.openintents.org/en/node/94
            // If a barcode is found, Barcodes returns RESULT_OK to
            // onActivityResult() of the app which requested the scan via
            // startSubActivity(). The barcodes contents can be retrieved with
            // intent.getStringExtra("SCAN_RESULT"). If the user presses Back,
            // the result code will be RESULT_CANCELED.

            startActivityForResult(intent, 4);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            new AlertDialog.Builder(this)
                    .setTitle("WARNING:")
                    .setMessage(
                            "You don't have Barcode Scanner installed. Please install it.")
                    .setCancelable(false)
                    .setNeutralButton("Install it now",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    Uri uri = Uri
                                            .parse("market://search?q=pname:com.google.zxing.client.android");
                                    startActivity(new Intent(
                                            Intent.ACTION_VIEW, uri));
                                }
                            }).show();

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        // super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 4) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                Toast.makeText(this, "Vous avez scanné " + contents, Toast.LENGTH_LONG).show();
            }
        }
    }
}
